
--drop table NCC;
--drop table KH;
--drop table NV;
--drop table lichlam;
--drop table CV;
--drop table CTKho;
--drop table SP;
--drop table KHO;
--drop table CN;
--drop table HDN;
--drop table HDB;
--drop table CTHDB;
--drop table CTHDN;


create table NCC
(
maNCC varchar(15) primary key not null,
tenNCC  nvarchar2(30) not null,
diachiNCC nvarchar2(30)
);
create table SP
(
maSP varchar(5) primary key,
tenSP nvarchar2(10),
DVT int ,
NSX date not null,
HSD date not null,
mota nvarchar2(20)
);

create table KH
(
maKH varchar(10) primary key,
tenKH nvarchar2(20) not null,
diachiKH nvarchar2(30),
sdt varchar2(11) not null
);

create table CN
(
maCN varchar(10) primary key,
tenCN varchar(20) not null,
diachiCN nvarchar2(30) not null
);

create table CV
(
maCV varchar(10) primary key,
tenCV varchar(20) not null,
ngayNC date not null,
ngayKT date not null
);

create table NV
(
maNV varchar(10) primary key,
tenNV varchar(20) not null,
diachiNV nvarchar2(30),
gioitinh nvarchar2(5),
ngaysinh date not null,
sdt varchar2(11) not null,
maCN varchar(10),
maCV varchar(10),
CONSTRAINT FK_NV_CN FOREIGN key (maCN) REFERENCES CN(maCN),
CONSTRAINT FK_NV_CV FOREIGN key (maCV) REFERENCES CV(maCV)
);

create table Kho
(
maKho varchar(10) primary key,
tenKho nvarchar2(20),
maCN varchar(10),
constraint FK_KHO_CN FOREIGN key (maCN) REFERENCES CN(maCN)
);

create table CTKho(
maKho varchar(10),
maSP varchar(5),
constraint FK_CTKHO_Kho FOREIGN key (maKho) REFERENCES Kho(maKho),
constraint FK_CTKHO_SP FOREIGN key (maSP) REFERENCES SP(maSP),
soluongSP int);

create table lichlam(
maNV varchar(10),
maCN varchar(10),
calam int,
ngaylam date,
constraint PK_lich primary key (maNV,maCN)
);

create table HDN(
maHDN varchar(15)not null PRIMARY KEY,
maNV varchar(10)not null,
maNCC varchar(15)not null,
tongtien int not null,
ngaynhap date not null,
constraint Fk_HDN_NV FOREIGN key (maNV) REFERENCES NV(maNV),
constraint Fk_HDN_NCC FOREIGN key (maNCC) REFERENCES NCC(maNCC)
);

create table HDB(
maHDB varchar(15)not null primary key,
maNV varchar(10)not null,
maKH varchar(10)not null,
Vat float,
tongtien int not null,
ngayban date not null,
constraint Fk_HDB_NV FOREIGN key (maNV) REFERENCES NV(maNV),
constraint Fk_HDB_KH FOREIGN key (maKH) REFERENCES KH(maKH)
);

create table CTHDN(
maHDN varchar(15),
maSP varchar(5),
SLN int,
don_gia_nhap int,
thanhtien int,
constraint FK_CTHDN_HDN FOREIGN key (maHDN) REFERENCES HDN(maHDN),
constraint FK_CTHDN_SP FOREIGN key (maSP) REFERENCES SP(maSP));

create table CTHDB(
maHDB varchar(15),
maSP varchar(5),
SLB int,
don_gia_ban int,
thanhtien int,
constraint FK_CTHDB_HDN FOREIGN key (maHDB) REFERENCES HDB(maHDB),
constraint FK_CTHDB_SP FOREIGN key (maSP) REFERENCES SP(maSP));



